package za.co.infowarestudios.customer.registration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CustomerRegistrationMicroserviceApplication.class)
@WebAppConfiguration
public class CustomerRegistrationMicroserviceApplicationTests {

	@Test
	public void contextLoads() {
	}

}
