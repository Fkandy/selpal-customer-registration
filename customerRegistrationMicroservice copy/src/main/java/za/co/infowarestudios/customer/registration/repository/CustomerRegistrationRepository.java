package za.co.infowarestudios.customer.registration.repository;

import org.springframework.data.repository.CrudRepository;
import za.co.infowarestudios.customer.registration.entity.Customer;

/**
 * Created by Goje on 2015-12-08.
 */
public interface CustomerRegistrationRepository extends CrudRepository<Customer,Long> {
    public Customer findByEmail(String email);
}
