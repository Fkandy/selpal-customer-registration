package za.co.infowarestudios.customer.registration.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Goje on 2015-12-08.
 */
@Entity
@Table(name = "customers")
public class Customer {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "customerID")
    public Long id;

    @Column(name="customerFirstname")
    public String firstname;

    @Column(name = "customerLastname")
    public  String lastname;

    @Column(name = "customerIdNumber")
    public String idNumber;
	
	@Column(name="customerSimCardSerialNumber")
	public String customerSimCardSerialNumber;
	
	
	@Column(name="customerICCID")
	public String customerICCID;//sim card number
	
	@Column(name="customerSelPalCellNumber")
	public String customerSelPalCellNumber;
	
	@Column(name ="customerHomeNumber")
	public String customerHomeNumber;
	
	@Column(name ="customerWorkNumber")
	public String customerWorkNumber;

    @Column(name ="customerStreet")
    public String customerStreet;

    @Column(name ="customerSurbub")
    public String customerSurbub;

    @Column(name ="customerCity")
    public String customerCity;

    @Column(name ="customerCode")
    public String customerCode;

    @Column(name ="customerEmail")
    public String customerEmail;

    @Column(name ="customerNetwork")
    public String customerNetwork;

    @Column(name ="customerPrimaryNumber")
    public String customerPrimaryNumber;

    @Column(name ="customerNationality")
    public String customerNationality;

    @Column(name ="customerEthnicGroup")
    public String customerEthnicGroup;

    @Column(name ="customerPrefferedLanguage")
    public String customerPrefferedLanguage;

    @Column(name ="customerMaritalStatus")
    public String customerMaritalStatus;


    @Column(name ="customerSpouceCellNumber")
    public String customerSpouceCellNumber;

    @Column(name ="customerEmployeeName")
    public String customerEmlployeeName;

    @Column(name ="customerOccupation")
    public String customerOccupation;

    @Column(name ="customerResidence")
    public String customerResidence;

    @ManyToOne
    @JoinColumn(name = "fk_marchandantID")
    private long fk_marchandantID;

    @ManyToOne
    @JoinColumn(name = "fk_operatorID")
    private long fk_operatorID;

    @Column(name = "checkedIdDocument")
    public boolean checkedIdDocument;

    @Column(name = "checkedProofOfAddress")
    public boolean checkedProofOfAddress;

    @Column(name = "userCode")
    public boolean userCode;

    @Column(name = "isSelPalSim")
    public boolean isSelPalSim;

    @JsonIgnore
    public String pin;


    public Customer(String firstname, String lastname, String idNumber, String customerSimCardSerialNumber, String customerICCID, String customerSelPalCellNumber, String customerHomeNumber, String customerWorkNumber, String customerStreet, String customerSurbub, String customerCity, String customerCode, String customerEmail, String customerNetwork, String customerPrimaryNumber, String customerNationality, String customerEthnicGroup, String customerPrefferedLanguage, String customerMaritalStatus, String customerSpouceCellNumber, String customerEmlployeeName, String customerOccupation, String customerResidence, long fk_marchandantID, long fk_operatorID, boolean checkedIdDocument, boolean checkedProofOfAddress, boolean userCode, boolean isSelPalSim, String pin, boolean isActive) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.idNumber = idNumber;
        this.customerSimCardSerialNumber = customerSimCardSerialNumber;
        this.customerICCID = customerICCID;
        this.customerSelPalCellNumber = customerSelPalCellNumber;
        this.customerHomeNumber = customerHomeNumber;
        this.customerWorkNumber = customerWorkNumber;
        this.customerStreet = customerStreet;
        this.customerSurbub = customerSurbub;
        this.customerCity = customerCity;
        this.customerCode = customerCode;
        this.customerEmail = customerEmail;
        this.customerNetwork = customerNetwork;
        this.customerPrimaryNumber = customerPrimaryNumber;
        this.customerNationality = customerNationality;
        this.customerEthnicGroup = customerEthnicGroup;
        this.customerPrefferedLanguage = customerPrefferedLanguage;
        this.customerMaritalStatus = customerMaritalStatus;
        this.customerSpouceCellNumber = customerSpouceCellNumber;
        this.customerEmlployeeName = customerEmlployeeName;
        this.customerOccupation = customerOccupation;
        this.customerResidence = customerResidence;
        this.fk_marchandantID = fk_marchandantID;
        this.fk_operatorID = fk_operatorID;
        this.checkedIdDocument = checkedIdDocument;
        this.checkedProofOfAddress = checkedProofOfAddress;
        this.userCode = userCode;
        this.isSelPalSim = isSelPalSim;
        this.pin = pin;
        this.isActive = isActive;
    }

    @Column(name = "isActive")
    public boolean isActive;
	
	



    @OneToMany(mappedBy = "customers")
    public Set <SimCard> bookmarks = new HashSet<>();

    //@ManyToOne


    public Customer() {}//for JPA




    //@ManyToOne(mappedBy \\)
}
