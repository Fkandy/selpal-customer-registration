package za.co.infowarestudios.customer.registration.services;

import org.springframework.beans.factory.annotation.Autowired;
import za.co.infowarestudios.customer.registration.entity.Customer;
import za.co.infowarestudios.customer.registration.repository.CustomerRegistrationRepository;

/**
 * Created by Goje on 2015-12-08.
 */
public class CustomerService {
    @Autowired
    CustomerRegistrationRepository customerRegistrationRepository;

    public Customer registerCutomer(Customer customer){
        //put to the database
        return customerRegistrationRepository.save(customer);
    }

    public Customer getCustomerByEmail(String email){
        //get method, get customer from the database
        return customerRegistrationRepository.findByEmail(email);

    }

    public Customer updateCustomerDetails(Customer customer){
        //post, updates the database
        return customerRegistrationRepository.save(customer);

    }

}
